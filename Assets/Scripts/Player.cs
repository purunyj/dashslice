﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        List<Touch> touches = InputHelper.GetTouches();
        int count = touches.Count;
        if (count == 0) return; //할 일이 없다.
        for (int i = 0; i < count ; i++ ){
            Touch touch = touches[i];
            Debug.Log("id:" + touch.fingerId + ",phase:" + touch.phase);
        }
    }
}
